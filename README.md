# Objective-C

Test project with:

* **Language:** Objective-C
* **Package Manager:** N/A
* **Framework:** N/A

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported                 |
|---------------------|---------------------------|
| SAST                | :white_check_mark:        |
| Dependency Scanning | :x:                       |
| Container Scanning  | :x:                       |
| DAST                | :x:                       |
| License Management  | :x:                       |

## Notes

Examples taken from test files on https://github.com/MobSF/test_files/
